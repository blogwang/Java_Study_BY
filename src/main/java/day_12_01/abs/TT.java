package day_12_01.abs;

/**
 * @author soberw
 */
public abstract class TT {

    String some = "hello";

    public static void talk() {
        System.out.println("talk...");
    }

    public void say() {
        System.out.println("say..." + some);
    }
}

class FF extends TT{
}
class FFTest{
    public static void main(String[] args) {
        TT.talk();
        TT f = new FF();
        f.say();
    }
}
