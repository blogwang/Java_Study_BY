package day_11_23;

/**
 * @author soberw
 */
public class Printf {
    public static void main(String[] args) {
        //格式化输出实例
        System.out.printf("我是%s,性别%c,今年%d岁%n", "张三", '男', 18);
        //我是张三,性别男,今年18岁

        //%n 换行类同 \n
        //%s 格式化字符串
        System.out.printf("我的名字是：%s\n", "张三");
        //我的名字是：张三

        //%d  格式化数字（十进制）
        System.out.printf("张三今年%d岁%n", 18);
        //张三今年18岁
        //%o  格式化数字（八进制）
        System.out.printf("%o%n", 63);
        //77
        //%x  格式化数字（十六进制）
        System.out.printf("%x%n", 255);
        //ff

        //%c  格式化字符型j  (可用字符对应的ASCII码替换)
        System.out.printf("%c%n", 97);
        //a
        System.out.printf("%c%n", 'a');
        //a

        //%b   格式化布尔值
        System.out.printf("%b%n", true);
        //true

        //%f   格式化浮点型
        //%.xf   x为数字，表示要保留几位小数，如%.2f为保留两位小数(会四舍五入)
        System.out.printf("%f%n", 1.555);
        //1.555000
        System.out.printf("%.2f%n",1.2555);
        //1.26

    }
}
