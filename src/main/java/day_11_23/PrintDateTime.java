package day_11_23;


/**
 * @author soberw
 */
public class PrintDateTime {
    public static void main(String[] args) {
        //举例输出：当前日期，格式为 "2021年11月23日 14:04:03 星期二"
        long d = System.currentTimeMillis();

        System.out.printf("%tY年%tm月%td日 %tT %tA ",d,d,d,d,d);
//        2021年11月23日 14:33:27 星期二
        System.out.println();
        System.out.printf("%1$tY年%1$tm月%1$td日 %1$tT %1$tA",d);
//        2021年11月23日 14:33:27 星期二
    }
}
