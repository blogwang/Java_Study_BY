package day_11_23;

import java.util.Calendar;
import java.util.Date;

/**
 * @author soberw
 */
public class PrintfDate {
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();

        //%tc  包括全部日期信息
        System.out.printf("%tc%n", date);
        //周二 11月 23 13:39:27 CST 2021

        //%tF  "年-月-日"格式
        System.out.printf("%tF%n", date);
        //2021-11-23

        //%tD  "月/日/年"格式
        System.out.printf("%tD%n", date);
        //11/23/21

        //%tr   "HH:MM:SS PM"格式(12小时制)
        System.out.printf("%tr%n", date);
        //01:43:04 下午

        //%tT   "HH:MM:SS"格式(24小时制)
        System.out.printf("%tT%n", date);
        //13:43:04

        //%tR   "HH:MM"格式 (24小时制)
        System.out.printf("%tR%n", date);
        //13:44

        //%tH   小时(2位数字24小时制，不足补零)
        System.out.printf("%tH%n", date);
        //13

        //%tI   小时(2位数字12小时制，不足补零)
        System.out.printf("%tI%n", date);
        //01

        //%tk   小时(2位数字24小时制，不补零)
        System.out.printf("%tk%n", date);
        //13

        //%tl   小时(2位数字12小时制，不补零)
        System.out.printf("%tl%n", date);
        //1

        //%tM   分钟(2位数字分钟，不足补零)
        System.out.printf("%tM%n", date);
        //53

        //%tS   秒(2位数字秒，不足补零)
        System.out.printf("%tS%n", date);
        //14

        //%tL   毫秒(3位数字毫秒，不足补零)
        System.out.printf("%tL%n", date);
        //699

        //%tN   毫秒(9位数字毫秒，不足补零)
        System.out.printf("%tN%n", date);
        //699000000

        //%tp 上午或者下午
        System.out.printf("%tp%n",date);
        //下午

        //%ts  1970-1-1 00:00:00到现在经过的秒数
        System.out.printf("%ts%n",date);
        //1637646987

        //%tQ  1970-1-1 00:00:00到现在经过的毫秒数
        System.out.printf("%tQ%n",date);
        //1637646987635

        //%tA  星期
        System.out.printf("%tA%n",date);
        //星期二

    }
}
