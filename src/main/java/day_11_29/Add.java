package day_11_29;

/**
 * @author soberw
 */
public class Add {
    public int add(int... number) {
        int sum = 0;
        for (int i : number) {
            sum += i;
        }
        return sum;
    }
}
