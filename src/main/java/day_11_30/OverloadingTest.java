package day_11_30;

/**
 * @author soberw
 */
public class OverloadingTest {
    public static void main(String[] args) {
        Overloading ol = new Overloading();
        ol.sum();
        ol.sum(1);
        ol.sum("str");
        ol.sum(1,3);
    }
}
