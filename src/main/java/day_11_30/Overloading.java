package day_11_30;

/**
 * @author soberw
 */
public class Overloading {

    /**
     * 以下方法构成重载
     */
    //初始方法
    public void sum() {
        System.out.println("空参数");
    }

    //构成重载，因为参数列表不同
    public void sum(int i) {
        System.out.println("int i");
    }
    //报错,因为参数列表相同
//    public void sum(int a){
//
//    }

    //构成重载，因为参数类型不同
    public void sum(String i) {
        System.out.println("String i");
    }

    //报错  重载跟方法有无返回值无关系
//    public int sum(int i){
//
//    }
    //构成重载 ，因为参数列表和长度都不同
    public void sum(int i, int j) {
        System.out.println("int i,int j");
    }
    //报错 重载与修饰符无关
//    protected void sum(int i){
//
//    }
}
