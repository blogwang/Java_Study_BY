package day_12_14.zuoye;

import java.io.File;

/**
 * @author soberw
 * @Classname CodeWordsCountTest
 * @Description 对CodeWordsCount的测试
 * @Date 2021-12-14 20:36
 */
public class CodeWordsCountTest {
    public static void main(String[] args) {

//        String path = "C:\\Users\\soberw\\Desktop\\src";
        String path = "E:\\HelloJava\\Java_Study_Beiyou\\src";
        String show = ReadAndWriterCode.show(new File(path));
        System.out.println(CodeWordsCount.show(new File(show)));
    }
}
