package day_12_14.zuoye;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author soberw
 * @Classname ToDoFile
 * @Description 实现文件删除，更名、文件复制、移动（剪切，粘贴）
 * @Date 2021-12-14 11:00
 */
public class ToDoFile {
    //用于复制文件时
    public static int index = 1;

    @Test
    public void deleteFile() {
        //文件删除操作
        File file = new File("a.txt");
        if (file.exists()) {
            file.delete();
        } else {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void renameFile() {
        //文件更名操作
        File f = new File("b.txt");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        f.renameTo(new File("bb.txt"));

    }

    @Test
    public void copyFile() {
        //借用系统方法：（推荐）
//        InputStream 类中的transferTo(OutputStream out)方法
//        public long transferTo(OutputStream out) throws IOException {
//            Objects.requireNonNull(out, "out");
//            long transferred = 0;
//            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
//            int read;
//            while ((read = this.read(buffer, 0, DEFAULT_BUFFER_SIZE)) >= 0) {
//                out.write(buffer, 0, read);
//                transferred += read;
//            }
//            return transferred;
//        }


//        //文件复制操作
//        //思路：先获取文件，读取字节流，然后指定将要复制的位置，将字节写入文件，如果此目录下有同名的文件了，则将复制的文件命名为副本文件
//
//        //第一步：获取
//        byte[] b;
//        String s = null;
//        String filename = "hello.txt";
//        if (new File(filename).exists()) {
//            try (FileInputStream fis = new FileInputStream(filename)) {
//                b = fis.readAllBytes();
//                s = new String(b, StandardCharsets.UTF_8);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
////        System.out.println(s);  //测试获取成功
//
//        //第二步：复制
//        //指定盘符：
//        File moveTo = new File("D:\\hello\\");
//        if (moveTo.exists()) {
//            File[] files = moveTo.listFiles((x, y) -> y.matches(filename));
//            if (files == null) {
//                try (FileOutputStream fos = new FileOutputStream(moveTo + filename, false)) {
//                    fos.write(s.getBytes(StandardCharsets.UTF_8));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                String sub = filename.substring(0, filename.lastIndexOf("."));
//                String ff = filename.replace(sub, sub + "副本" + index++);
//                try (FileOutputStream fos = new FileOutputStream(moveTo + ff, false)) {
//                    fos.write(s.getBytes(StandardCharsets.UTF_8));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }


    }


}
