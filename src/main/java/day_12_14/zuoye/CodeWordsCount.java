package day_12_14.zuoye;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author soberw
 * @Classname CodeWordsCount
 * @Description 统计一个文件中出现的所有词汇的个数, 并输出在文件所在目录下的"codeWordsCount.txt"中去
 * @Date 2021-12-14 20:36
 */
public class CodeWordsCount {
    /**
     * @param file 指定文件
     * @description: 统计一个文件中出现的所有词汇的个数, 并以Map返回,若不是文件，或者指定文件不存在，或者空文件，或者没有单词，则返回空集合
     * @return: Map<String, Integer>  String是单词，Integer是个数
     * @author: soberw
     * @time: 2021/12/14 20:44
     */
    private static Map<String, Integer> codeWordsCount(File file) {
        Map<String, Integer> map = new HashMap<>();
        Pattern p = Pattern.compile("[a-z]+", Pattern.CASE_INSENSITIVE);
        if (file.exists() && file.isFile()) {
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                br.lines().forEach(e -> {
                    Matcher m = p.matcher(e);
                    while (m.find()) {
                        String s = m.group();
                        if (map.containsKey(s)) {
                            map.put(s, map.get(s) + 1);
                        } else {
                            map.put(s, 1);
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return map;
    }

    /**
     * @param file 指定文件
     * @description: 输出在文件所在目录下的"codeWordsCount.txt"中去
     * @return: 是否输出成功<boolean>
     * @author: soberw
     * @time: 2021/12/14 21:23
     */

    public static boolean show(File file) {
        int count = 0;
        //存放数据
        Map<String, Integer> map = codeWordsCount(file);
        if (map.size() == 0) {
            return false;
        }
        //存放键（单词）
        List<String> list = new ArrayList<>(map.keySet());
        //按照单词个数降序
        list.sort((a, b) -> map.get(b).compareTo(map.get(a)));
//        for (String s : list) {
//            System.out.printf("%s:%03d个\n", s, map.get(s));
//        }

        //将Map写入文件中去
        try (FileOutputStream fos = new FileOutputStream(new File(file.getParentFile(), "codeWordsCount.txt"), false)) {
            for (String s : list) {
                fos.write(String.format("%d、%s  : 出现 %d 次\r\n", count++, s, map.get(s)).getBytes(StandardCharsets.UTF_8));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
