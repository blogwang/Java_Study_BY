package day_12_14.zuoye;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author soberw
 * @Classname CreateFile
 * @Description 建立一个文件，并写入内容。读取一个文本文件。
 * @Date 2021-12-14 10:55
 */
public class IOFile {
    public static void main(String[] args) {
        //建立一个文件，并写入内容  FileOutputStream

        //此种书写方式为try-with-catch 语句，一般用于IO流操作，将文件写入或者读取写在try()内，可以实现文件的自动关闭，避免手动关闭，避免资源浪费

        //写入操作  FileOutputStream(path,append)   path为将要添加的文件路径名称（可以是FIle类型，可以是String），append确定是否为追加模式
        try (FileOutputStream fos = new FileOutputStream("a.txt", true)) {
            fos.write("hello".getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //读取操作  FileInputStream
        try (FileInputStream fis = new FileInputStream("a.txt")) {
            //获取一个文件的全部字节（文本格式，或者叫ASCII格式）,返回为byte数组
            byte[] b = fis.readAllBytes();
            //转换为字符串输出，第二个参数表示编码格式
            System.out.println(new String(b, StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test() {
        //思考：每隔一秒在一个文件内写入当前时间，并输出,共用时10秒
        //实现：
        for (int i = 0; i < 10; i++) {
            long start = System.currentTimeMillis();
            long time;
            while (true) {
                long end = System.currentTimeMillis();
                if (end - start == 1000) {
                    time = end;
                    try (FileOutputStream fos = new FileOutputStream("time.txt", true); FileInputStream fis = new FileInputStream("time.txt")) {
                        fos.write(String.format("%tF %<tT\r\n", time).getBytes(StandardCharsets.UTF_8));
                        fos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }

        }
    }
}
