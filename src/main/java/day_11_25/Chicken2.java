package day_11_25;

/**
 * 百钱百鸡2
 *
 * @author soberw
 */
public class Chicken2 {
    public static void main(String[] args) {
        for (int i = 1; i < 20; i++) {
            for (int j = 1; j < 33; j++) {
                int k = 100 - i - j;
                //当三种鸡的价格总和为100时 并且小鸡个数是正数也要是3的倍数
                if (5 * i + 3 * j + k / 3 == 100 && k % 3 == 0) {
                    System.out.printf("公鸡%d 母鸡%d 小鸡%d\n", i, j, k);
                }
            }
        }
    }
}
