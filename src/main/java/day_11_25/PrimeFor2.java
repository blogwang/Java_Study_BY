package day_11_25;

/**
 * 计算1-100之间的素数和
 *
 * @author soberw
 */

public class PrimeFor2 {
    public static void main(String[] args) {
        int sum = 0;
        //声明互斥锁
        boolean flag = true;
        int counter = 0;
        for (int i = 2; i <= 100; i++) {
            flag = true;
            //偶数直接跳到下一次
            if (i != 2 && i % 2 == 0){
                continue;
            }
            //从2到除了它本身的数之间判断
            for (int j = 2; j < i; j++) {
                counter++;
                //有因子直接退出
                if (i % j == 0) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                sum += i;
            }
        }
        System.out.println("总和为" + sum);
        System.out.println("循环了" + counter + "次");
    }

}
