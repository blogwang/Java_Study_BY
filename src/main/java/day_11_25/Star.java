package day_11_25;

import java.util.Scanner;

/**
 * 打印菱形星号
 *
 * @author soberw
 */
public class Star {
    public static void main(String[] args) {
        int max = 8;
        int spa = max;
        for (int i = 1; i < max; i += 2) {
            --spa;
            for (int k = 1; k < spa; k++) {
                System.out.print(" ");
            }
            String ss = "";
            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int i = max - 3; i >= 1; i -= 2) {
            ++spa;
            for (int k = 1; k < spa; k++) {
                System.out.print(" ");
            }

            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
