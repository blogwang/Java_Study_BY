package day_11_25;

import java.util.Arrays;
import java.util.Random;

/**
 * 冒泡排序
 *
 * @author soberw
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] arr = {2, 6, 8, 0, 1, 4, 9, 3, 5, 7};
        System.out.println("排序前：" + Arrays.toString(arr));
        //排序
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println("排序后：" + Arrays.toString(arr));
        //乱序
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            int ran = arr[random.nextInt(arr.length)];
            int temp = arr[i];
            arr[i] = arr[ran];
            arr[ran] = temp;
        }
        System.out.println("乱序后：" + Arrays.toString(arr));
    }
}
