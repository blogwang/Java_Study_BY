package day_11_25;

/**
 * 百钱白鸡
 * 鸡翁一值钱五，鸡母一值钱三，鸡雏三值钱一。百钱买百鸡，问鸡翁、鸡母、鸡雏各几何？
 *
 * @author soberw
 */
public class Chicken {
    public static void main(String[] args) {
        for (int i = 1; i < 20; i++) {
            for (int j = 1; j < 33; j++) {
                for (int k = 0; k < 100; k++) {
                    if (5 * i + 3 * j + k == 100 && i + j + 3 * k == 100) {
                        System.out.printf("100文钱共买得公鸡%d只，母鸡%d只，小鸡%d只。%n", i, j, 3 * k);
                    }
                }
            }
        }
    }
}
