package day_11_25;

/**
 * 计算1-100之间的素数和
 *
 * @author soberw
 */

public class PrimeFor {
    public static void main(String[] args) {
        //记录和
        int sum = 0;
        //记录因子个数
        int count = 0;
        int counter = 0;
        for (int i = 2; i <= 100; i++) {
            //初始置0
            count = 0;
            for (int j = 1; j <= i; j++) {
                counter++;
                if (i % j == 0) {
                    count++;
                }
            }
            //两个因子为素数
            if (count == 2) {
                sum += i;
            }

        }
        System.out.println("总和为" + sum);
        System.out.println("循环了" + counter + "次");
    }

}
