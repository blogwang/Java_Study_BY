package day_11_25;

import java.time.LocalDate;
import java.util.Scanner;

/**
 * @author soberw
 */
public class Switch {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入年份：");
        int year = sc.nextInt();
        System.out.print("请输入月份：");
        int month = sc.nextInt();
        int day = 31;
        switch (month) {
            case 4:
            case 6:
            case 9:
            case 11:
                day = 30;
                break;
            case 2:
                day = (LocalDate.of(year, month, 1).isLeapYear()) ? 29 : 28;
                break;
            default:
                break;
        }
        System.out.printf("%d年%d月有%d天%n", year, month, day);
    }
}
