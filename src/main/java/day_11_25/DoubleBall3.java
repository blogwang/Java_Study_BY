package day_11_25;

import java.util.Random;

/**
 * 双色球
 * 红球1~33  篮球1~16
 *
 * @author soberw
 */
public class DoubleBall3 {
    public static void main(String[] args) {
        Random random = new Random();
        //放红球
        int[] red = new int[6];
        for (int i = 0; i < red.length; i++) {
            while (true) {
                boolean flag = true;
                int redRAn = random.nextInt(33) + 1;
                for (int j = 0; j < red.length; j++) {
                    if (red[j] == redRAn) {
                        flag = false;
                    }
                }
                if (flag) {
                    red[i] = redRAn;
                    break;
                }
            }
        }
        System.out.print("{ ");
        for (int i : red) {
            System.out.printf("[%02d] ", i);
        }
        System.out.print("}");
        System.out.printf("{ [%02d] }", random.nextInt(16) + 1);
    }
}
