package day_11_25;

import java.util.Random;
import java.util.Scanner;

/**
 * 猜数字游戏
 *
 * @author soberw
 */
public class GuessNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random random = new Random();
        //正确数字
        int trueNum = (int) (Math.round(Math.random() * 99) + 1);
        // 记录次数
        int index = 0;
        System.out.println("猜数字游戏！猜1~100内的整数。");
        System.out.println("----------------------------");
        while (true) {
            System.out.print("请输入数字：");
            if (!sc.hasNextInt()) {
                System.out.println("无效输入！！！请重新输入");
                sc.next();
                continue;
            }
            int guess = sc.nextInt();
            if (guess < 1 || guess > 100) {
                System.out.println("不在范围内！！！请重新输入");
                continue;
            } else if (guess > trueNum) {
                System.out.printf("%d、太大了\n", ++index);
            } else if (guess < trueNum) {
                System.out.printf("%d、太小了\n", ++index);
            } else {
                System.out.printf("恭喜你猜对了！你的成绩是：%d", 100 - (10 * index));
                break;
            }
            if (index > 9) {
                System.out.println("小笨猪！");
            }
        }
    }
}
