package day_12_16;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author soberw
 * @Classname QRCode
 * @Description
 * @Date 2021-12-16 19:47
 */
public class QRCode {
    public static void main(String[] args) throws WriterException, IOException {
        BitMatrix bm = new QRCodeWriter().encode("123", BarcodeFormat.QR_CODE, 200, 200);
        BufferedImage i = MatrixToImageWriter.toBufferedImage(bm);
        ImageIO.write(i, "png", new FileOutputStream("d:\\er.png"));
    }
}
