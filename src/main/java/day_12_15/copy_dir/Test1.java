package main.java.day_12_15.copy_dir;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author soberw
 * @Classname Test
 * @Description
 * @Date 2021-12-15 20:33
 */
public class Test1 {
    public static Map<String, String> map = new HashMap<>();

    public static void main(String[] args) throws IOException {
        getFile(new File("E:\\HelloJava\\Java_Study_Beiyou\\src\\day_12_13"));
        System.out.println(map);

    }

    public static void getFile(File file) {
        if (file.exists()) {
            if (file.isFile()) {
                map.put(file.getAbsolutePath(), file.getName());
            } else if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File f : files) {
                    getFile(f);
                }
            }
        }
    }
}
