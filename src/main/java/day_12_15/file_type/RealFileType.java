package day_12_15.file_type;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author soberw
 * @Classname RealFileType
 * @Description 获取文件真实的格式
 * @Date 2021-12-15 11:21
 */
public class RealFileType {
    /**
     * @param file 传入的文件
     * @description: 传入一个文件获取真实类型
     * @return: 文件的类型（若不是文件或者文件不存在则返回null）
     * @author: soberw
     * @time: 2021/12/15 13:08
     */
    public static String getFileType(File file) {
        String type = "未知类型";
        if (file.exists() && file.isFile()) {
            Map<String, String> map = SomeFileType.FILE_TYPE_MAP;
            List<String> list = new ArrayList<>(map.keySet());
            byte[] b = new byte[4];
            StringBuilder sbu = new StringBuilder();
            try (FileInputStream fis = new FileInputStream(file)) {
                fis.read(b);
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (byte b1 : b) {
                sbu.append(String.format("%02x", b1));
            }
            for (String s : list) {
                if (map.get(s).startsWith(sbu.toString().toUpperCase())) {
                    type = s;
                }
            }
        } else {
            return null;
        }
        return type;
    }

    @Test
    public void test() {
        String path = "C:\\Users\\soberw\\Desktop\\1.jpg";
//        path = "C:\\Users\\soberw\\Desktop\\1.txt";
        path = "G:\\jdk\\jdk1.8.0_301\\src.zip";
        path = "E:\\HelloJava\\Java_Study_Beiyou\\src\\day_12_15\\file_type\\RealFileType.java";
        File file = new File(path);
        System.out.println(getFileType(file));
    }
}
