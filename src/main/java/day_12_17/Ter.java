package day_12_17;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * @author soberw
 * @Classname Ter
 * @Description
 * @Date 2021-12-17 17:01
 */
public class Ter {
    public static void main(String[] args) throws IOException {
        Random rand = new Random();
        int w = 160, h = 60, t = 2;
        BufferedImage i = new BufferedImage(w, h, t);
        var g = i.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);

        String text = str();
        Font font = new Font("宋体", Font.BOLD, 50);
//        try {
//            font = Font.createFont(0, new File("fonts/ComicbookSmash.ttf"));
            font = font.deriveFont(Font.BOLD, 50);
            g.setFont(font);
//        } catch (FontFormatException e) {
//            e.printStackTrace();
//        }
        String textmask = str(15);
        for (int j = 0; j < textmask.length(); j++) {
            g.setColor(new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256), rand.nextInt(80) + 80));
            font = font.deriveFont(Font.BOLD, rand.nextInt(10) + 20);
            g.setFont(font);
            String strc = String.valueOf(textmask.charAt(j));
            int x = rand.nextInt(w);
            int y = rand.nextInt(h) + 20;
            g.drawString(strc, x, y);
        }
        //干扰曲线
        int linesize = rand.nextInt(5) + 1;
        for (int i1 = 0; i1 < linesize; i1++) {
            paint(g, w, h);
        }
        for (int j = 0; j < text.length(); j++) {
            g.setColor(getRandColor());
            font = font.deriveFont(Font.BOLD, rand.nextInt(20) + 30);
            g.setFont(font);
            String strc = String.valueOf(text.charAt(j));
            int x = j * 38 + 6;
            int y = rand.nextInt(35) + 20;
            double radianPercent = Math.PI * (rand.nextInt(35) / 180D);
            if (rand.nextBoolean())
                radianPercent = -radianPercent;
            g.rotate(radianPercent, x, y);
            g.drawString(strc, x, y);
            g.rotate(-radianPercent, x, y);
        }


        g.dispose();
        String name = text + ".png";
        ImageIO.write(i, "png",new File("E:\\HelloJava\\Java_Study_Beiyou\\src\\main\\java\\ver.png"));
        System.out.println(text);
        Runtime run = Runtime.getRuntime();
        run.exec("cmd /k start E:\\HelloJava\\Java_Study_Beiyou\\src\\main\\java\\ver.png");
    }

    public static Color getRandColor() {
        Random rand = new Random();
        return new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
    }

    public static Color getRandColorAlpha() {
        Random rand = new Random();
        return new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
    }

    public static String str(int len) {
        String letter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random rand = new Random();
        StringBuilder sbu = new StringBuilder();
        for (int i = 0; i < len; i++) {
            sbu.append(letter.charAt(rand.nextInt(letter.length())));
        }
        return sbu.toString();
    }

    public static String str() {
        return str(4);
    }

    public static void paint(Graphics2D g, int w, int h) {
        Random rand = new Random();
        g.setStroke(new BasicStroke(rand.nextInt(3) + 3));
        g.setColor(new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256), rand.nextInt(5) + 1));//设置画线的颜色
        double y = 0;
        double x = 0;
        int a = rand.nextInt(50) + 10;
        int b = rand.nextInt(15) + 5;
        int c = rand.nextInt(150) + 30;

        //一个周期
        for (x = 0; x <= w; x += 0.1) {
            y = Math.sin(x * Math.PI / c);//转化为弧度,1度=π/180弧度
            //y = (200 + 5 * y);//便于在屏幕上显示
            y = (a + b * y);
            //g.drawString(".",(int)x,(int)y);//用这种方式也可以
            g.drawLine((int) x, (int) y, (int) x, (int) y);//画点
        }
    }

}
