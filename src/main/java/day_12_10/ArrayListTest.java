package day_12_10;

import java.util.ArrayList;
import java.util.List;

/**
 * @author soberw
 * @Classname ArrayListTest
 * @Description
 * @Date 2021-12-10 16:51
 */
public class ArrayListTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(List.of("ghah","hfgae"));
        System.out.println(list);
    }
}
