package day_12_06.day;

/**
 * @author soberw
 * @Classname Test
 * @Description
 * @Date 2021-12-06 13:01
 */
public class Test {
    public static void main(String[] args) {
        StringBuilder sdu = new StringBuilder();
        sdu.append("abba");
        System.out.println(sdu);
        System.out.println(sdu.reverse());
        System.out.println(sdu.toString().equals(sdu.reverse().toString()));
    }
}
