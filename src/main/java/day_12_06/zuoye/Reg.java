package day_12_06.zuoye;

import java.util.Scanner;

/**
 * @author soberw
 * @Classname Reg
 * @Description
 * @Date 2021-12-06 10:57
 */
public class Reg {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        // 1. 判断字符串有没有大写字母
        System.out.print("请输入字符串：");
        String az = sc.nextLine();
        System.out.println(az.matches(".*[A-Z]+.*") ? "有大写字母" : "没有大写字母");

        // 2. 判断字符串有没有汉字
        System.out.print("请输入字符串：");
        String cn = sc.nextLine();
        System.out.println(cn.matches(".*[\\u4e00-\\u9fa5].*") ? "有汉字" : "没有汉字");

        // 3. 判断字符串有没有数字
        System.out.print("请输入字符串：");
        String num = sc.nextLine();
        System.out.println(num.matches(".*[0-9].*") ? "有数字" : "没有数字");

        // 4. 判断一个字符串是不是标签的手机号
        System.out.print("请输入字符串：");
        String photo = sc.nextLine();
        System.out.println(photo.matches("^(13[0-9]|14[5|7]|15[0-9]|18[0-9])\\d{8}$") ? "是手机号" : "不是手机号");

    }
}
