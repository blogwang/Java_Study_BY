package leetcode.day_12_06;

/**
 * 给你一个字符串 s，找到 s 中最长的回文子串。
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "babad"
 * 输出："bab"
 * 解释："aba" 同样是符合题意的答案。
 * 示例 2：
 * <p>
 * 输入：s = "cbbd"
 * 输出："bb"
 * 示例 3：
 * <p>
 * 输入：s = "a"
 * 输出："a"
 * 示例 4：
 * <p>
 * 输入：s = "ac"
 * 输出："a"
 *
 * @author soberw
 * @Classname LongestPalindrome0005
 * @Description
 * @Date 2021-12-06 12:39
 */
public class LongestPalindrome0005 {
    /**
     * @param s 需要查找的字符串
     * @description: 找到 s 中最长的回文子串。
     * @return: 最长回文子串
     * @author: soberw
     * @time: 2021/12/6 12:40
     */
    public String longestPalindrome(String s) {
        //直接返回
        if (s.length() == 0 || s.length() == 1) {
            return s;
        }
        int len = s.length();
        //定义回文子串的起止下标
        int start = 0;
        int end = 0;
        //定义两个指针（头尾）
        int left = 0;
        int right = 0;
        for (int i = 0; i < len; i++) {
            left = i;
            right = i;
            //从当前位置开始，判断尾指针后面的字符是否与当前所指相等，相等继续向后比较
            //旨在找到连续字符
            while (right < len - 1 && s.charAt(right) == s.charAt(right + 1)) {
                right++;
            }
            //在连续字符的基础上，判断其前一个是否和后一个相等，以此类推
            while (left > 0 && right < len - 1 && s.charAt(left - 1) == s.charAt(right + 1)) {
                left--;
                right++;
            }
            //始终保持start和end是最大的范围
            if ((end - start) < (right - left)) {
                start = left;
                end = right;
            }
        }
        return s.substring(start, end + 1);
    }

    public static void main(String[] args) {
        System.out.println(new LongestPalindrome0005().longestPalindrome("babad"));
        System.out.println(new LongestPalindrome0005().longestPalindrome("cbbd"));
        System.out.println(new LongestPalindrome0005().longestPalindrome(""));
        System.out.println(new LongestPalindrome0005().longestPalindrome("a"));
    }
}
