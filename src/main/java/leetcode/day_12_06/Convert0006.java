package leetcode.day_12_06;

import java.util.ArrayList;
import java.util.List;

/**
 * 将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。
 * <p>
 * 比如输入字符串为 "PAYPALISHIRING"行数为 3 时，排列如下：
 * <p>
 * P   A   H   N
 * A P L S I I G
 * Y   I   R
 * 之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："PAHNAPLSIIGYIR"。
 * <p>
 * 请你实现这个将字符串进行指定行数变换的函数：
 * <p>
 * string convert(string s, int numRows);
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "PAYPALISHIRING", numRows = 3
 * 输出："PAHNAPLSIIGYIR"
 * 示例 2：
 * 输入：s = "PAYPALISHIRING", numRows = 4
 * 输出："PINALSIGYAHRPI"
 * 解释：
 * P     I    N
 * A   L S  I G
 * Y A   H R
 * P     I
 * 示例 3：
 * <p>
 * 输入：s = "A", numRows = 1
 * 输出："A"
 *
 * @author soberw
 * @Classname Convert0006
 * @Description
 * @Date 2021-12-06 21:01
 */
public class Convert0006 {
    /**
     * @param s       字符串
     * @param numRows 行数
     * @description: 将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。
     * @return: 排列后的字符串 String
     * @author: soberw
     * @time: 2021/12/6 21:02
     */
    public String convert1(String s, int numRows) {
        //思路一，二维数组，按规律插入，按规律取出
        char[] cs = s.toCharArray();
        List<List<String>> listArr = new ArrayList<>();
        int i = 0;
        int j = 0;
        while (true) {
            if (i < numRows) {
                i++;
            }
            break;
        }


        return "";
    }

    public String convert2(String s, int numRows) {
        //思路二，是按行号循环遍历，找到他们所有元素的对应的下标规律(与行号之间)


        return "";
    }
}
