package leetcode.day_12_05;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 给定两个大小分别为 m 和 n 的正序（从小到大）数组nums1 和nums2。请你找出并返回这两个正序数组的 中位数 。
 * <p>
 * 算法的时间复杂度应该为 O(log (m+n)) 。
 * 示例 1：
 * <p>
 * 输入：nums1 = [1,3], nums2 = [2]
 * 输出：2.00000
 * 解释：合并数组 = [1,2,3] ，中位数 2
 * 示例 2：
 * <p>
 * 输入：nums1 = [1,2], nums2 = [3,4]
 * 输出：2.50000
 * 解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5
 * 示例 3：
 * <p>
 * 输入：nums1 = [0,0], nums2 = [0,0]
 * 输出：0.00000
 * 示例 4：
 * <p>
 * 输入：nums1 = [], nums2 = [1]
 * 输出：1.00000
 * 示例 5：
 * <p>
 * 输入：nums1 = [2], nums2 = []
 * 输出：2.00000
 *
 * @author soberw
 * @Classname FindMedianSortedArrays0004
 * @Description
 * @Date 2021-12-05 21:54
 */
public class FindMedianSortedArrays0004 {
    /**
     * @param array int数组
     * @description: 将int数组转换为List<Integer>
     * @return: List<Integer>
     * @author: soberw
     * @time: 2021/12/5 22:10
     */

    public List<Integer> listOf(int[] array) {
        List<Integer> result = new ArrayList<>();
        for (int i : array) {
            result.add(i);
        }
        return result;
    }

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        double middle = 0.0;
        List<Integer> l1 = listOf(nums1);
        List<Integer> l2 = listOf(nums2);
        l2.addAll(l1);
        l2.sort(Comparator.comparingInt(a -> a));
        if (l2.size() % 2 == 0) {
            middle = ((double) l2.get(l2.size() / 2) + (double) l2.get(l2.size() / 2 - 1)) / 2;
        } else {
            middle = (double) l2.get(l2.size() / 2);
        }
        return middle;
    }
}
