package leetcode.day_12_03;

/**
 * 给你两个非空 的链表，表示两个非负的整数。它们每位数字都是按照逆序的方式存储的，并且每个节点只能存储一位数字。
 * <p>
 * 请你将两个数相加，并以相同形式返回一个表示和的链表。
 * <p>
 * 你可以假设除了数字 0 之外，这两个数都不会以 0开头。
 * <p>
 * 示例 1：
 * <p>
 * 输入：l1 = [2,4,3], l2 = [5,6,4]
 * 输出：[7,0,8]
 * 解释：342 + 465 = 807.
 * 示例 2：
 * <p>
 * 输入：l1 = [0], l2 = [0]
 * 输出：[0]
 * 示例 3：
 * <p>
 * 输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * 输出：[8,9,9,9,0,0,0,1]
 *
 * @author soberw
 */
public class AddTwoNumbers0002 {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //创建一个空的链表，指定一个头指针，可以是任何数，空着也行，这里我写的0
        ListNode ahead = new ListNode(0);
        //将用于存放链表的数据，我们都知道这里的赋值是复制的地址值，所以他们其实指向同一个地址
        ListNode current = ahead;
        //用于存放十位（即如果7+8=15  carry就是存放十位的1的）
        int carry = 0;
        //循环的终止条件是如果两个参数链表都为空
        while (l1 != null || l2 != null) {
            //创建两个变量分别存放值，如果已经走到头了就赋值为0
            int x = l1 != null ? l1.val : 0;
            int y = l2 != null ? l2.val : 0;
            //计算和，这里加上carry可能不好理解，其实就是简单的数学知识，个位数上的10在十位数上就是1
            //就是说在个位数加上10，相当于在十位数加上1
            int sum = x + y + carry;
            //转换为十位数
            carry = sum / 10;
            //存放在预定的链表中（注意格式）
            current.next = new ListNode(sum % 10);
            //链表向后走，指向下一个空指针
            current = current.next;
            //如果后面还有元素就向后走
            if (l1 != null) {
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
        }
        //最后剩余的一位不能漏
        if (carry != 0) {
            current.next = new ListNode(carry);
        }
        //要知道头结点从来都没变过，我们变得只是current的指向，所以我们返回头结点的next就行了
        return ahead.next;
    }

}


class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}

