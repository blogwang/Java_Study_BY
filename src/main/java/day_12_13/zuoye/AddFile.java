package day_12_13.zuoye;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author soberw
 * @Classname AddFile
 * @Description 创建文件或者目录
 * @Date 2021-12-13 18:57
 */
public class AddFile {
    public static void main(String[] args) {
        //创建目录
        File file = new File("d:/hello/abc");
        if (!file.exists()) {
            file.mkdirs();
            System.out.println("创建成功！");
        }
        //在刚创建的目录中在创建一个文件
        File file1 = new File("d:/hello/abc/hello.txt");
        try {
            file1.createNewFile();
        } catch (IOException e) {
            System.out.println("创建失败！");
        }

        if (file1.exists()) {
            file1.delete();
            System.out.println("删除成功！");
        }
    }
    @Test
    public void test(){
        File f = new File("c:/xxx");
        if(!f.exists()){
            f.mkdirs();
        }
    }
}
