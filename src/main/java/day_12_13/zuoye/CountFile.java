package day_12_13.zuoye;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;

/**
 * @author soberw
 * @Classname CountFile
 * @Description 递归统计某目录中有多少java文件？每个Java文件中有多少行代码，总有多少行代码
 * @Date 2021-12-13 16:17
 */
public class CountFile {
    //统计个数
    public static int count = 0;
    //统计行数
    public static int lines = 0;

    public static void main(String[] args) throws IOException {
//        countJavaFile(new File("E:\\HelloJava\\Java"));
//        countJavaFile(new File("G:\\"));
//        countJavaFile(new File("E:\\HelloJava\\Java_Study_Beiyou"));
        countJavaFile(new File("C:\\Users\\soberw\\Desktop\\src"));
        System.out.printf("共%d个java文件，共%d行代码", count, lines);
    }

    public static void countJavaFile(File file) throws IOException {
        if (file.exists()) {
            if (file.isFile() && file.getName().endsWith(".java")) {
                count++;
                //读取出来但是会报错，不稳定，不推荐
                //int line = Files.readAllLines(Path.of(file.getPath())).size();

                //稳定读取
                long line = new BufferedReader(new FileReader(file)).lines().count();
                lines += line;
                System.out.printf("%s(%s)【%tF %<tT】共%d行\n", file.getParentFile(), file.getName(), file.lastModified(), line);
            } else if (file.isDirectory() && file.list() != null) {
                for (File listFile : Objects.requireNonNull(file.listFiles())) {
                    countJavaFile(listFile);
                }
            }
        }
    }
}
