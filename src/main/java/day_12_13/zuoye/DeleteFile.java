package day_12_13.zuoye;

import java.io.File;

/**
 * @author soberw
 * @Classname DeleteFile
 * @Description  删除非空目录
 * @Date 2021-12-13 11:32
 */
public class DeleteFile {
    public static void main(String[] args) {
        File file = new File("E:\\HelloJava\\Java_Study_Beiyou\\aaa");
        deleteFile(file);
    }

    public static void deleteFile(File file) {
        if (file.exists()) {
            File[] files = file.listFiles();
            if (files != null) {
                if (files.length == 0) {
                    file.delete();
                } else {
                    for (File f : files) {
                        deleteFile(f);
                    }
                }
            }
        }
        file.delete();
    }
}
