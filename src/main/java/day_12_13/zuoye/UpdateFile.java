package day_12_13.zuoye;

import java.io.File;

/**
 * @author soberw
 * @Classname UpdateFile
 * @Description 对文件名进行修改操作
 * @Date 2021-12-13 20:14
 */
public class UpdateFile {
    public static void main(String[] args) {
        File file = new File("D:\\hello\\abc\\soberw.txt");
        File newFile = new File("D:\\hello\\abc\\sober.txt");
        if (file.exists()) {
            file.renameTo(newFile);
        }
        File moveFile = new File("D:\\aaa\\bbb\\ccc.txt");
        if(file.getParentFile().exists()){
            newFile.renameTo(moveFile);
        }
        File files = new File("D:\\aaa\\bbb");
        files.renameTo(new File("D:\\aaa\\abc"));
    }
}
