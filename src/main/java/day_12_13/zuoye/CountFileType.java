package day_12_13.zuoye;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author soberw
 * @Classname CountFileType
 * @Description 递归统计某目录，并列出些目录中，某文件类型的文件个数？例如：包含.java文件30个，.jpg文件78个等
 * @Date 2021-12-13 16:20
 */
public class CountFileType {
    /**
     * 声明一个Map存储文件类型及次数，key对应文件类型，value对应出现次数
     */
    private static Map<String, Integer> map = new HashMap<>();

    /**
     * @description: 列出给定目录中，某文件类型的文件个数，以及文件类型个数
     * @param str: 传入的路径
     * @return: void
     * @author: soberw
     * @time: 2021/12/13 18:16
     */
    public static void show(String str) {
        File file = new File(str);
        if (file.exists()) {
            countFileType(file);
            Set<String> keySet = map.keySet();
            System.out.println("本目录下包含有：");
            int count = 0;
            for (String s : keySet) {
                count += map.get(s);
                System.out.printf("%s文件：%d个。\n", s, map.get(s));
            }
            System.out.printf("共%d种文件类型，共%d个文件。", keySet.size(), count);
        }else {
            System.out.println("请检查路径是否合法！");
        }
    }


    /**
     * @param file 文件路径
     * @description: 递归检索目录，获取文件名后缀并存入map中，value会自增一
     * @return: void
     * @author: soberw
     * @time: 2021/12/13 18:17
     */
    private static void countFileType(File file) {
        if (file.exists()) {
            if (file.isFile() && file.getName().contains(".")) {
                String type = file.getName().substring(file.getName().lastIndexOf(".")).toLowerCase();
                if (!map.containsKey(type)) {
                    map.put(type, 1);
                } else {
                    if (map.get(type) != null) {
                        map.put(type, map.get(type) + 1);
                    }
                }
            } else if (file.isDirectory() && file.list() != null) {
                for (File listFile : Objects.requireNonNull(file.listFiles())) {
                    countFileType(listFile);
                }
            }
        }
    }
}
