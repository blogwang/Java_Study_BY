package day_12_02;

/**
 * @author soberw
 */
public class Student {
    //默认初始化
    int id;
    //显示初始化
    int age = 18;
    String name = "张三";


    //构造器重新赋值
    public Student() {
        name = "李四";
    }

    //代码块中赋值
    {
        name = "王五";
    }

    //提供set方法
    public void setAge(int age) {
        this.age = age;
    }
}

class StudentTest {
    public static void main(String[] args) {
        Student student = new Student();
        System.out.println("id:" + student.id);
        System.out.println("age:" + student.age);
        System.out.println("name:" + student.name);
        //通过set方法对age属性重新赋值
        student.setAge(20);
        System.out.println("age:" + student.age);
        //通过对象.属法对name属性重新赋值
        student.name = "马六";
        System.out.println("name:" + student.name);
    }


}
