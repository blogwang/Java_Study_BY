package day_12_02.zuoye;

import java.util.Arrays;

/**
 * 对StringArrays的测试
 *
 * @author soberw
 */
public class SATest {
    public static void main(String[] args) {
        StringArrays sa = new StringArrays();
        String[] str = {"eat", "tea", "tan", "ate", "nat", "bat"};
        String[][] newStr = new String[str.length][str.length];
        for (int i = 0; i < str.length; i++) {
            for (int j = 0; j < str.length; j++) {
                if (sa.compare(str[i], str[j])) {
                    newStr[i][j] = str[j];
                }
            }
        }

        for (String[] s : newStr) {
            System.out.println(Arrays.toString(s));
        }

    }
}
