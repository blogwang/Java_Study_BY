package day_12_02.zuoye;

/**
 * String file = "c:/user/abc/upload/xxsfasf.afasf-asfas.jpg";
 * //求出文件目录 c:/user/abc/upload/
 * //求出文件名称 xxsfasf.afasf-asfas.jpg
 * //求出文件的扩展名 jpg
 * //将字符串替换为c:/user/abc/upload/20211202154333.jpg
 * <p>
 * 此类的功能是，给一个文件地址类型的字符串，调用不同方法求出文件目录，名称，拓展名等...
 * 此类不提供空参构造器
 *
 * @author soberw
 */
public class Files {
    private String file;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Files(String file) {
        this.file = file;
    }

    /**
     * 求文件目录
     *
     * @param fileAddress 文件地址名
     * @return 文件目录
     */
    public String dir(String fileAddress) {
        String s = fileAddress.contains("/") ? "/" : "\\";
        int index = fileAddress.lastIndexOf(s);
        return fileAddress.substring(0, index + 1);
    }

    /**
     * 求构造器传入文件地址的文件目录
     *
     * @return 文件目录
     */
    public String dir() {
        return dir(this.file);
    }

    /**
     * 求文件名称
     *
     * @param fileAddress 文件地址名
     * @return 文件名称
     */
    public String fileName(String fileAddress) {
        String s = fileAddress.contains("/") ? "/" : "\\";
        int index = fileAddress.lastIndexOf(s);
        return fileAddress.substring(index + 1);
    }

    /**
     * 求构造器传入文件地址的文件名称
     *
     * @return 文件名称
     */
    public String fileName() {
        return fileName(this.file);
    }

    /**
     * 求文件拓展名，目前仅支持最长10位拓展名
     *
     * @param fileAddress 文件地址
     * @return 文件后缀名
     */
    public String suffix(String fileAddress) {
        int i = file.length() <= 11 ? 5 : 11;
        String str = fileAddress.substring(fileAddress.length() - i);
        if (str.contains(".")) {
            int index = str.lastIndexOf(".");
            return str.substring(index + 1);
        } else {
            return "当前文件无后缀名或后缀名过长...";
        }
    }

    /**
     * 求构造器传入文件的拓展名，目前仅支持最长10位拓展名
     *
     * @return 文件后缀名
     */
    public String suffix() {
        return suffix(this.file);
    }

    /**
     * 将给定地址中的文件名替换为给定的新文件名，返回替换后的文件地址
     *
     * @param fileAddress 文件地址
     * @param replaceFile 新文件名
     * @return 替换后的文件地址
     */

    public String replaceFileWith(String fileAddress, String replaceFile) {
        String s = fileAddress.contains("/") ? "/" : "\\";
        int start = fileAddress.lastIndexOf(s);
        int end = fileAddress.lastIndexOf(".");
        return fileAddress.replace(fileAddress.substring(start + 1, end), replaceFile);
    }

    /**
     * 将构造器地址中的文件名替换为给定的新文件名，返回替换后的文件地址
     *
     * @param replaceFile 新文件名
     * @return 替换后的文件地址
     */

    public String replaceFileWith(String replaceFile) {
        return replaceFileWith(this.file, replaceFile);
    }

}
