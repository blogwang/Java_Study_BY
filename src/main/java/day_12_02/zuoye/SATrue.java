package day_12_02.zuoye;

import java.util.*;

/**
 * @author soberw
 */
public class SATrue {
    public static void main(String[] args) {
        String[] strs = {"ate", "tae", "tan", "ant", "eat","bat"};
        System.out.println(new SATrue().grouper(strs));
    }

    public List<List<String>> grouper(String[] strs) {
        if (strs == null || strs.length == 0) {
            return new ArrayList<>();
        }
        Arrays.sort(strs);
        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] c = str.toCharArray();
            Arrays.sort(c);
            String sortedStr = String.valueOf(c);
            if (!map.containsKey(sortedStr)) {
                map.put(sortedStr, new ArrayList<>());
            }
            map.get(sortedStr).add(str);
        }
        return new ArrayList<>(map.values());
    }
}

