package day_12_02.zuoye;

import java.util.List;
import java.util.Vector;

/**
 * @author soberw
 */
public class Test {
    public static void main(String[] args) {
        Vector<String> vector = new Vector<>();
        vector.add("1111");
        vector.add("2111");
        vector.add("3111");
        vector.add("4111");
        vector.add("5111");
        vector.add("6111");
        System.out.println(List.of(vector));
        for (int i = 0; i < vector.size(); i++) {
            System.out.println(vector.get(i));
            vector.set(i,null);
        }
        System.out.println(List.of(vector));

        String str = "hello";
        str = str.replaceFirst("l","");
        System.out.println(str);

    }
}
