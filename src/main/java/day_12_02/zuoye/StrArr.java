package day_12_02.zuoye;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author soberw
 */

public class StrArr {
    /**
     * 对字符串排序
     *
     * @param str 字符串
     * @return 排序后的
     */
    private static String sortStr(String str) {
        char[] s = str.toCharArray();
        Arrays.sort(s);
        return String.valueOf(s);
    }

    public static void main(String[] args) {
        //原始数组
        String[] str = {"ate", "tae", "tan", "ant", "eat", "bat"};
        //中间集合
        Set<String> set = new HashSet<>();
        //存放最终结果
        Set<Set<String>> setLast = new HashSet<>();
        for (int i = 0; i < str.length; i++) {
            set.add(sortStr(str[i]));
        }
        for (String s : set) {
            //暂存数据
            Set<String> seter = new HashSet<>();
            //排序后相等的元素放入临时集合
            for (int i = 0; i < str.length; i++) {
                if (s.equalsIgnoreCase(sortStr(str[i]))) {
                    seter.add(str[i]);
                }
            }
            setLast.add(seter);
        }
        System.out.println(setLast);
    }
}
