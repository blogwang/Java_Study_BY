package day_12_02;

/**
 * @author soberw
 */
public class Person {

    //代码块中赋值
    {
        id = 1;
    }
    //声明一个属性
    int id = 0;
}
class PersonTest{
    public static void main(String[] args) {
        Person p = new Person();
        System.out.println(p.id);
    }
}
