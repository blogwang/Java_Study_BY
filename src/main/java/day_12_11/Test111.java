package day_12_11;

/**
 * @author soberw
 * @Classname Test111
 * @Description
 * @Date 2021-12-11 17:10
 */
public class Test111 {
    public static void main(String[] args) {
        System.out.println(1 << 2);
        System.out.println(4 >> 1);
        System.out.println(-1 << 1);

        StringBuilder sbu = new StringBuilder();
        StringBuffer sb = new StringBuffer();
        sbu.append("aaa");
        sbu.append(1).append(2).append(3);
        sb.append("aaa");
    }
}
