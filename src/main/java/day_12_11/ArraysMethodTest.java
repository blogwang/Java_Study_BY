package day_12_11;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Spliterator;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;

/**
 * @author soberw
 * @Classname ArraysMethodTest
 * @Description
 * @Date 2021-12-11 9:06
 */
public class ArraysMethodTest {

    public static void main(String[] args) {
        String[] strs = {"java", "private", "public", "protected", "final", "interface", "extends", "implements"};
//        String[] strs1 = {"java", "private", "public", "protected", "final", "interface", "extends", "implements"};
//        System.out.println(Arrays.hashCode(strs));
//        Arrays.fill(strs,"hello");
        System.out.println(Arrays.toString(strs));
//        System.out.println(Arrays.equals(strs,strs1));//true
        List<String> ss = Arrays.asList("hello", "world");
        System.out.println(ss);
        ss.add("haha");
        System.out.println(ss);
    }

    @Test
    public void asListTest() {
        List<String> ss = Arrays.asList("hello", "world");
//        List<String> ss1 = Arrays.asList("hello", "world",1);   报错，类型必须一直（泛型）
        System.out.println(ss);  //[hello, world]

//        ss.add("java");  //UnsupportedOperationException  会报错
//        ss.remove(1);   //UnsupportedOperationException  会报错

        System.out.println(ss.get(0));  //hello
        ss.set(0, "java");
        System.out.println(ss);  //[java, world]

    }

    @Test
    public void toStringTest() {
        String[] str = {"java", "hello", "javascript"};
        String[][] strs = {{"a", "b"}, {"c"}, {"d", "e"}};
        System.out.println(Arrays.toString(str));
        //[java, hello, javascript]

        System.out.println(Arrays.toString(strs));
        //[[Ljava.lang.String;@4563e9ab, [Ljava.lang.String;@11531931, [Ljava.lang.String;@5e025e70]
        //普通的toString()方法只转化一层，内层还是地址值

        System.out.println(Arrays.deepToString(strs));
        //可以深度转换
        //[[a, b], [c], [d, e]]
    }

    @Test
    public void sortTest() {
        String[] str = {"abc", "add", "java", "hello", "javascript"};
        int[] ii = {1, 8, 99, 222, 35};

        System.out.println(Arrays.toString(str));
        System.out.println(Arrays.toString(ii));

        //单参数情况
        //Arrays.sort(str);  默认全排，字母会按照字母表顺序
        //Arrays.sort(ii);
        //System.out.println(Arrays.toString(str));  //[abc, add, hello, java, javascript]
        //System.out.println(Arrays.toString(ii));  //[1, 8, 35, 99, 222]

        //多参数情况
        //Arrays.sort(str,2,4);   只排列指定范围内的
        //Arrays.sort(ii,2,4);
        //System.out.println(Arrays.toString(str));  //[abc, add, hello, java, javascript]
        //System.out.println(Arrays.toString(ii));  //[1, 8, 99, 222, 35]

        //可传入lambda表达式(多参数情况可指定开始结束位置)
//        Arrays.sort(str, (a, b) -> b.compareTo(a));  //降序
//        System.out.println(Arrays.toString(str));  //[javascript, java, hello, add, abc]


        //parallelSort()方法目前我实验感觉与sort()相差无几，基本相似
        Arrays.parallelSort(str);
        System.out.println(Arrays.toString(str));  //[abc, add, hello, java, javascript]

        Arrays.parallelSort(str, (a, b) -> b.compareTo(a));
        System.out.println(Arrays.toString(str));  //[javascript, java, hello, add, abc]
    }

    @Test
    public void binarySearchTest() {
        int[] a = {1, 5, 7, 4, 6, 7, 8, 4, 9, 0};
        Arrays.sort(a);  //必须先排序
        System.out.println(Arrays.toString(a));
        //[0, 1, 4, 4, 5, 6, 7, 7, 8, 9]
        System.out.println(Arrays.binarySearch(a, 4));  //2
        System.out.println(Arrays.binarySearch(a, 11));  //-11
        System.out.println(Arrays.compare(a, new int[]{1, 3}));
    }

    @Test
    public void compareTest() {
        int[] a = {1, 2, 3, 4};
        int[] b = {1, 2, 3, 4, 5};
        int[] c = {1, 2, 3, 4, 5};
        String[] s1 = {"java", "hello", "c++"};
        String[] s2 = {"java", "hello"};

//        System.out.println(Arrays.compare(a,b));  //-1
//        System.out.println(Arrays.compare(b,a));  //1
//        System.out.println(Arrays.compare(b,c));  //0

        System.out.println(Arrays.compare(s1, s2));  //1
        //也可划定范围去比较，或者传入lambda


//        System.out.println(Arrays.compareUnsigned(s1,s2));//报错
        System.out.println(Arrays.compareUnsigned(a, b));  //-1
    }

    @Test
    public void copyOfTest() {

        //copyOf

        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        int[] arr1 = Arrays.copyOf(arr, 5);  //拷贝长度为5，第二个参数为新数组的长度
        int[] arr2 = Arrays.copyOf(arr, 15);
        System.out.println(Arrays.toString(arr1));  //[1, 2, 3, 4, 5]
        System.out.println(Arrays.toString(arr2));  //[1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0]

        arr[0] = 20;   //改变原数组
        System.out.println(Arrays.toString(arr));     //原数组改变
        System.out.println(Arrays.toString(arr1));   //复制后的数组不变

        String[] str = {"java", "hello", "world"};
        String[] str1 = Arrays.copyOf(str, 2);
        String[] str2 = Arrays.copyOf(str, 5);
        System.out.println(Arrays.toString(str1));  //[java, hello]
        System.out.println(Arrays.toString(str2));  //[java, hello, world, null, null]


        //copyOfRange()

        int[] arrs = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        int[] arr3 = Arrays.copyOfRange(arrs, 2, 8);   //[3, 4, 5, 6, 7, 8]
        int[] arr4 = Arrays.copyOfRange(arrs, 5, 15);  //[6, 7, 8, 9, 0, 0, 0, 0, 0, 0]
        System.out.println(Arrays.toString(arr3));
        System.out.println(Arrays.toString(arr4));
        arrs[6] = 99;  //改变原数组
        System.out.println(Arrays.toString(arrs));   //[1, 2, 3, 4, 5, 6, 99, 8, 9, 0]  原数组改变
        System.out.println(Arrays.toString(arr3));   //[3, 4, 5, 6, 7, 8]  复制后的不会随着改变

    }

    @Test
    public void equalsTest() {
        int[] a = {1, 2, 3, 4, 5, 6};
        int[] b = {6, 5, 3, 4, 2, 1};
        Integer[] aa = {1, 2, 3, 4, 5, 6};
        Integer[] bb = {6, 5, 3, 4, 2, 1};

        System.out.println(Arrays.equals(a, b));   //false
        System.out.println(Arrays.equals(a, 2, 3, b, 2, 3));  //指定比较范围  true
    }

    @Test
    public void deepEqualsTest() {
        String[] s1 = {"java", "hello", "c++"};
        String[] s2 = {"java", "hello"};
        String[] s3 = {"java", "hello", "c++"};
        System.out.println(Arrays.deepEquals(s1, s2));  //false
        System.out.println(Arrays.deepEquals(s1, s3));  //true

        String[][] s4 = {{"hello"}, {"java"}, {"c++"}, {"python"}};
        String[][] s5 = {{"hello"}, {"java"}, {"c++"}, {"python"}};
        System.out.println(Arrays.deepEquals(s4, s5));  //true
        System.out.println(Arrays.equals(s4, s5));    //false

        int[] a = {1, 2};
        int[] b = {1, 2};
//        System.out.println(Arrays.deepEquals(a,b));  //报错

    }

    @Test
    public void fillTest() {
        String[] a = {"a", "b", "c", "d", "e", "f"};
        System.out.println(Arrays.toString(a));  //[a, b, c, d, e, f]
        Arrays.fill(a, "java");
        System.out.println(Arrays.toString(a));  //[java, java, java, java, java, java]

        String[] b = {"a", "b", "c", "d", "e", "f"};
        System.out.println(Arrays.toString(b));  //[a, b, c, d, e, f]
        Arrays.fill(b, 2, 5, "java");
        System.out.println(Arrays.toString(b));  //[a, b, java, java, java, f]

        //默认全填充，也可以指定范围，会改变原数组
    }

    @Test
    public void mismatchTest() {
        String[] s1 = {"java", "c++", "html", "css", "Javascript", "world"};
        String[] s2 = {"world", "c++", "html", "css", "Javascript"};
        System.out.println(Arrays.mismatch(s1, s2)); //0
        System.out.println(Arrays.mismatch(s1, 1, 4, s2, 1, 4));  //-1
        System.out.println(Arrays.mismatch(s1, 1, 5, s2, 1, 4));  //3

    }

    @Test
    public void parallelPrefixTest() {
        String[] s1 = {"java", "c++", "html", "css", "Javascript", "world"};
        System.out.println(Arrays.toString(s1));   //[java, c++, html, css, Javascript, world]
        Arrays.parallelPrefix(s1, String::concat);
        System.out.println(Arrays.toString(s1));   //[java, javac++, javac++html, javac++htmlcss, javac++htmlcssJavascript, javac++htmlcssJavascriptworld]

        int[] a = {1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(a));  //[1, 2, 3, 4, 5]
        Arrays.parallelPrefix(a, (x, y) -> x * y);
        System.out.println(Arrays.toString(a));  //[1, 2, 6, 24, 120]
        Arrays.parallelPrefix(a, 2, 4, (x, y) -> x / y);
        System.out.println(Arrays.toString(a));  //[1, 2, 6, 0, 120]  也可指定范围
    }

    @Test
    public void setAllTest() {
        int[] arr = {111, 222, 333, 444, 555};
        Arrays.setAll(arr, (e) -> e + 5);
        System.out.println(Arrays.toString(arr));   //[5, 6, 7, 8, 9]

        String[] str = {"a", "b", "c"};
        Arrays.setAll(str, new IntFunction<>() {
            @Override
            public String apply(int value) {
                return value + "java";
            }
        });
        System.out.println(Arrays.toString(str));  //[0java, 1java, 2java]

        int[] arr1 = {111, 222, 333, 444, 555};
        Arrays.parallelSetAll(arr1, new IntUnaryOperator() {
            @Override
            public int applyAsInt(int operand) {
                return operand + 5;
            }
        });
        System.out.println(Arrays.toString(arr1));   //[5, 6, 7, 8, 9]

        String[] str1 = {"a", "b", "c"};
        Arrays.parallelSetAll(str1, (m) -> m + "haha");
        System.out.println(Arrays.toString(str1));  //[0haha, 1haha, 2haha]


        //总结setAll和parallelSetAll差不多，且都支持lambda
        //通过索引值去改变元素，改编后的值与索引有关
    }
}
