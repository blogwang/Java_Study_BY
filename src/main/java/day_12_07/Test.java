package day_12_07;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author soberw
 * @Classname Test
 * @Description
 * @Date 2021-12-07 8:58
 */
public class Test {
    public static void main(String[] args) {
        String[] s = new String[5];
        System.out.println(Arrays.toString(s));
        var p = Pattern.compile("[a-c]{3}");
        var m = p.matcher("aaA");
        System.out.println(m.matches());
    }
    @org.junit.Test
    public void s(){
        String s = "<div>hello</div><html>hello</html><div>hello 15</div>";
        System.out.println(s.replaceAll("(<div>.*?)(hello)(.*?</div>)","$1abc$3"));
//        Pattern p = Pattern.compile("<div>.*?</div>");
//        Matcher m = p.matcher(s);
//        while(m.find()){
//            System.out.println(m.group());
//        }

    }
}
